# Aplicación chorra para explicar 12factors

## Uso

### Despliegue de la aplicación

```bash
$ docker-compose up --build
```

Tras el lanzamiento, la aplicación se estará sirviendo en <http://localhost:8888>

### Lanzamientos de comandos a la aplicación de django

```bash
$ docker-compose run app manage.py {comando de django} {parámetros}
```

